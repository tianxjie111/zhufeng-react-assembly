import React from "react";

import { Upload } from "./index";
import { withKnobs } from "@storybook/addon-knobs";
import styled from "styled-components";


export default {
	title: "Upload",
	component: Upload,
	decorators: [withKnobs],
};

export const knobsUpload = () => (
		<Upload
		></Upload>
);