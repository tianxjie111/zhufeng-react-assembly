
import React,
{   
    ReactNode,
    ReactElement,
    useState,
    useMemo,
    useEffect,
    useRef,
    PropsWithChildren,
    CSSProperties
  
    
} from "react";
import axios from "axios";

import Button from "../button";
export type UploadProps={

} 


export function Upload(props: PropsWithChildren<UploadProps>) {
	const inputRef = useRef<HTMLInputElement>(null);
	const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
		if (e.target.files && e.target.files.length <= 0) return;
		console.log(e.target.files);
		const formData = new FormData();
		formData.append("avatar", e.target.files![0]);
		axios.post("http://localhost:51111/user/uploadAvatar/", formData, {
			headers: {
				"Content-Type": "multipart/form-data",
			},
		});
	};
	const handleClick = () => {
		inputRef.current?.click();
	};

	return (
		<div>
			<input
				ref={inputRef}
				type="file"
				onChange={handleChange}
				style={{ display: "none" }}
				value=""
			></input>
			<Button onClick={handleClick}>upload</Button>
		</div>
	);
}