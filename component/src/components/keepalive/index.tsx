import React, {
    PropsWithChildren,
    useMemo,
    createContext,
    useState,
    useEffect,
    useRef,
    useContext,
  } from 'react';
  
  const Context = createContext({});
  
  export function AliveScope(props: any) {
    const [state, setState] = useState({});
    const ref: any = useMemo(() => {
      return {};
    }, []);
  
    const keep = useMemo(() => {
      return (id: any, children: any) =>
        new Promise(resolve => {
          setState({ [id]: { id, children } });
          setTimeout(() => {
            resolve(ref[id]);
          });
        });
    }, [ref]);
    debugger;
    return (
      <Context.Provider value={keep}>
        {props.children}
        {Object.values(state).map((v: any, i: number) => (
          <div key={v.id} ref={node => (ref[v.id] = node)}>
            {v.children}
          </div>
        ))}
      </Context.Provider>
    );
  }
  
  type KeepAliveProps = {
    id: string;
  };
  
  export function KeepAlive(props: PropsWithChildren<KeepAliveProps>) {
    const keep: any = useContext(Context);
    const ref: any = useRef(null);
  
    useEffect(() => {
      debugger;
      const init = async (data: any) => {
        const realContent = await keep(data.id, data.children);
        if (ref.current) {
          ref.current.appendChild(realContent);
        }
      };
      init(props);
    }, [props, keep]);
    return <div ref={ref}></div>;
  }